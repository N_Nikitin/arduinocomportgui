package gui;

import arduino.Arduino;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class MainFormGUI extends JDialog{
    private JLabel port;
    private JLabel baudRate;
    private JButton turnOnLightsButton;
    private JButton turnOffLightsButton;
    private JPanel content;

    MainFormGUI(String port, Integer baudRate) {
        this.port.setText(this.port.getText() + " " + port);
        this.baudRate.setText(this.baudRate.getText() + " " + baudRate);

        setContentPane(content);
        setModal(true);
        // call onCancel() when cross is clicked
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        turnOnLightsButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Arduino arduino = GUI.getArduino();
                arduino.serialWrite('1');
            }
        });
        turnOffLightsButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                Arduino arduino = GUI.getArduino();
                arduino.serialWrite('0');
            }
        });
    }

    private void onCancel() {
        Arduino arduino = GUI.getArduino();
        if (arduino != null) {
            arduino.serialWrite('0');
            arduino.closeConnection();
        }
        dispose();
    }
}
