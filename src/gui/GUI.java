package gui;

import arduino.Arduino;

import javax.swing.*;
import java.awt.event.*;

public class GUI extends JDialog {
    private JPanel contentPane;
    private JTextField comPortTextField;
    private JTextField baudRateTextField;
    private JButton sendButton;
    private JLabel wrongInputMessage;

    private String comPort;
    private Integer baudRate;
    private MainFormGUI mainFormGUI;

    static Arduino getArduino() {
        return arduino;
    }

    private static Arduino arduino;

    private GUI() {
        setContentPane(contentPane);
        setModal(true);

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(e -> onCancel(),
                KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        sendButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    comPort = comPortTextField.getText();
                    baudRate = Integer.valueOf(baudRateTextField.getText());
                    arduino = new Arduino(comPort, baudRate);
                    boolean connected = arduino.openConnection();
                    System.out.println("Соединение установлено: " + connected);
                    mainFormGUI = new MainFormGUI(comPort, baudRate);
                    initMainForm(mainFormGUI);
                    setVisible(false);
                } catch (Exception exc) {
                    wrongInputMessage.setVisible(true);
                    hideMainForm(mainFormGUI);
                    System.out.println(exc.toString());
                }
            }
        });
    }

    private void onCancel() {
        if (arduino != null) {
            arduino.closeConnection();
        }
        dispose();
    }

    private void initMainForm(MainFormGUI mainFormGUI) {
        mainFormGUI.setModal(true);
        mainFormGUI.pack();
        mainFormGUI.setVisible(true);
    }

    private void hideMainForm(MainFormGUI mainFormGUI) {
        mainFormGUI.setVisible(false);
    }

    public static void main(String[] args) {
        GUI dialog = new GUI();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }

}
